package com.company;

public enum Role {
    DIRECTOR,
    ADMINISTRATOR,
    MANAGER,
    CLIENT,
    ANONYMOUS
}
