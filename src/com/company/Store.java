package com.company;

public class Store {
    private String storeName;
    private String url;
    private User[] allUsers;
    private User authorizedUser;

    public Store(String storeName, String url, User[] allUsers) {
        this.storeName = storeName;
        this.url = url;
        this.allUsers = allUsers;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User[] getAllUsers() {
        return allUsers;
    }

    public void setAllUsers(User[] allUsers) {
        this.allUsers = allUsers;
    }

    public User getAuthorizedUser() {
        return authorizedUser;
    }

    public void setAuthorizedUser(User authorizedUser) {
        this.authorizedUser = authorizedUser;
    }


    public boolean login(String login, String password) {

        for (User user : allUsers) {

            if (login.equals(user.getLogin()) && password.equals(user.getPassword())) {

                authorizedUser = user;
                System.out.println("The user has logged in");
                return true;

            }

        }

        return false;

    }

    public void getCurrentUserRights() {

        if (authorizedUser == null) {
            return;
        }

        Role role = authorizedUser.getRole();

        switch (role) {
            case DIRECTOR: {
                System.out.println("Store director, can manage staff and prices");
                break;
            }
            case ADMINISTRATOR: {
                System.out.println("Store administrator, can add products and edit their description");
                break;
            }
            case MANAGER: {
                System.out.println("Store manager, can communicate with customers");
                break;
            }
            case CLIENT: {
                System.out.println("The customer of the store can buy goods and enjoy the discount");
                break;
            }
            case ANONYMOUS: {
                System.out.println("Anonymous user, can buy goods and / or log in");
                break;
            }
        }
    }

    public void logout() {
        authorizedUser = null;
        System.out.println("The user has logged out");
    }

}
