package com.company;

import java.util.Scanner;
import java.util.SortedMap;

public class Main {

    public static void main(String[] args) {

        User user1 = new User("User1", "434975yfy", "John", Role.DIRECTOR);
        User user2 = new User("User2", "fdgdsf436", "Anna", Role.ADMINISTRATOR);
        User user3 = new User("User3", "4tergftwt", "Boris", Role.MANAGER);
        User user4 = new User("User4", "b32yf87fy", "Mary", Role.CLIENT);
        User user5 = new User("User5", "f4e3tvce4", "noname", Role.ANONYMOUS);

        User[] allUsers = {user1, user2, user3, user4, user5};

        Store store = new Store("Store", "http://sonechko.ua", allUsers);

        while (true) {
            tryLogin(store);
        }

    }

    static void tryLogin(Store store) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter login: ");
        String login = scanner.nextLine();

        System.out.println("Enter the password:");
        String password = scanner.nextLine();

        boolean loggedIn = store.login(login, password);

        if (loggedIn == true) {  // if (loggedIn) {
            store.getCurrentUserRights();
            store.logout();
        } else {
            System.out.println("Incorrect login and\\or password");
        }
    }
}
